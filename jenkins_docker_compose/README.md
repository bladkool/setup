# Install and Run Jenkins With Docker Compose

## <sup>*</sup>Prerequisites
 - Docker
 - Docker Compose


## Do not use in production!
<i><b>This setup runs the containers as root. <!-- In a production environment, you would add a Jenkins user with a user ID $ \geq $ 1000 to the systems running Jenkins controllers and agents. --></b></i>

## Setup

### Docker compose
Create in your project directory a ```docker-compose.yml``` file with the following content:
```
services:
  jenkins:
    image: jenkins/jenkins:lts
    privileged: true
    user: root
    ports:
      - 8087:8080
      - 50000:50000
    container_name: jenkins
    volumes:
      - ./jenkins_home:/var/jenkins_home
      - /var/run/docker.sock:/var/run/docker.sock
```
<i><b>This setup will run the latest Jenkins image with root privileges.</b></i> The container is run with host networking, so Docker redirects ports 8087 and 50000 to the host’s network.

### Jenkins Controller
Run your Jenkins controller by running docker compose in the project directory. Open a shell and type: ```docker compose up -d```

Open a browser, go to http://localhost:8087. You will see the Jenkins Unlock page.


![Unlock Jenkins](./assets/jenkins001.png)

As you can read on the page the initial password can be found in a log file, but Jenkins prints the initial password to standard output too.

In the shell type: ```docker compose logs``` and scroll to the part which reads:
```
 
*************************************************************
*************************************************************
*************************************************************
 
Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:
 
d172c780218b5982a4505937747d5d7c
 
This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
 
*************************************************************
*************************************************************
*************************************************************
```
Copy and enter the password an click Continue.

Select Install Suggested Plugins on the next page.

![Customize Jenkins](./assets/jenkins002.png)

![Getting Started](./assets/jenkins003.png)

When Jenkins finishes, it will prompt you for a new admin user and password. Enter a user name and password and click Save and Continue.

![Create First Admin User](./assets/jenkins004.png)

On the next page you can change the host name of your controller. Accept the default and click Save and Finish.

![Instance Configuration](./assets/jenkins005.png)

The setup of Jenkins is completed, the next step is to set up an agent.

![Jenkins is ready!](./assets/jenkins006.png)

### Jenkins Agent
Before adding an agent you need to generate SSH keys. This will allow the controller to access the agent via SSH.

To generate a SSH key type in the shell: ```ssh-keygen -t ed25519 -f jenkins_agent -N '' -C 'Jenkins Agent' -q```

<i><b>Note: the SSH key is generated without a passhprase (-N '') in quit mode (-q)</b></i>

This command creates two files: jenkins_agent, which holds the private key, and jenkins_agent.pub, which holds the public key.

#### Add controller access to the Jenkins agent
In the browser go to the <b>Manage Jenkins</b> menu.

![Welcome to Jenkins!](./assets/jenkins007.png)

Go to <b>Manage Credentials</b>.

![Manage Credentials](./assets/jenkins008.png)

Click <b>Jenkins</b> under <b>Stores scoped to Jenkins</b>.

![Credentials](./assets/jenkins009.png)


Then click <b>Global credentials</b>.

![System](./assets/jenkins010.png)

Finally, click <b>Add Credentials</b> in the menu on the left.

![Add credentials
](./assets/jenkins011.png)

Set these options on this screen.

- <b>Kind</b>: Select SSH Username with private key.
- <b>Scope</b>: Limit the scope to System. This means the key can’t be used for jobs.
- <b>ID</b>: Give the credential an ID.
- <b>Description</b>: Provide a description.
- <b>Username</b>: Enter jenkins for a username. Don’t use the username used to create the key.
- Under <b>Private Key</b>,  check Enter directly.

Now, paste the contents of jenkins_agent in the text box.

![New credentials
](./assets/jenkins012.png)

Click on <b>Create</b>.

![Add credentials
](./assets/jenkins013.png)

#### Set up the Jenkins agent
Stop docker compose: ```docker compose down```.
Add a new service to the ```docker-compose.yml``` file in your project directory.
```
services:
  jenkins:
    image: jenkins/jenkins:lts
    privileged: true
    user: root
    ports:
      # - 8080:8080
      - 8087:8080
      - 50000:50000
    container_name: jenkins
    volumes:
      # - /home/${myname}/jenkins_compose/jenkins_configuration:/var/jenkins_home
      # - /home/hortus/projects/jenkins/jenkins_compose/jenkins_configuration:/var/jenkins_home
      - ./jenkins_configuration:/var/jenkins_home
      - /var/run/docker.sock:/var/run/docker.sock
  agent:
    image: jenkins/ssh-agent:jdk11
    privileged: true
    user: root
    container_name: agent
    expose:
      - 22
    environment:
      - JENKINS_AGENT_SSH_PUBKEY=ssh-ed25519 AADI/kbcbfXQilqEcx89FFYtneH1NTE5AAAAI1rgHjN7RKmSmDlpfNOAAC3NzaC1lZVH Jenkins Agent
``` 
Start docker compose: ```docker-compose up -d```. If closed, open a browser and go to http://localhost:8087. Click <b>Manage Jenkins</b>, and select <b>Manage Nodes and Clouds</b> instead of credentials.

![Welcome to Jenkins!
](./assets/jenkins014.png)

![Manage Jenkins
](./assets/jenkins015.png)

Click <b>New Node</b>.

![New Node
](./assets/jenkins016.png)

![New Node
](./assets/jenkins017.png)

Enter the <b>Node name</b> and select the <b>Type</b>.

![New Node
](./assets/jenkins018.png)

Click <b>Create</b>.

![New Node
](./assets/jenkins019.png)

In the form define your Jenkins agent:
 - name: give your agent a name
 - Remote root directory: /home/jenkins/agent

![New Node
](./assets/jenkins020.png)

 - under <b>Usage</b> select <b>Use this node as much as possible</b>
 - under <b>Launch</b> method, select <b>Launch agents via SSH</b>
 - for <b>Host</b>, enter <b>agent</b> (each container can reach the others by using their container names as hostnames)

![New Node
](./assets/jenkins021.png)

 - click the dropdown under 'Credentials' and select the key one you just defined
 - under 'Host Key Verification Strategy', select 'Non verifying Verification Strategy'

![New Node
](./assets/jenkins022.png)

 - click 'Advanced'
    - this opens the advanced configuration page. You need to change one setting here. Set the <b>JavaPath</b> to ```/opt/java/openjdk/bin/java```<sup>**</sup>

![New Node
](./assets/jenkins023.png)
![New Node
](./assets/jenkins024.png)
![New Node
](./assets/jenkins025.png)

 - click <b>Save</b> at the bottom

Go back to the node list and click on your new node name. Then click on Log in the menu on the left.

![Nodes
](./assets/jenkins026.png)
![New Node
](./assets/jenkins027.png)
![New Node
](./assets/jenkins028.png)
![New Node
](./assets/jenkins029.png)

If you see the message <b>Agent successfully connected and online</b> the agent is running.

<hr>

<sup>*</sup><b>Versions</b> \
&nbsp;&nbsp;&nbsp; - Jenkins 2.440.3 \
&nbsp;&nbsp;&nbsp; - Docker version 26.1.2 \
&nbsp;&nbsp;&nbsp; - Docker Compose version 2.27.0

<sup>**</sup><b>JavaPath</b> \
If an error indicates that the Java path not is found change the <b>JavaPath</b> setting to the path of the java binary. In this case the path ```/usr/local/openjdk-11/bin/java``` is not valid and causes the error.

![New Node
](./assets/jenkins030.png)
![New Node
](./assets/jenkins031.png)

<hr>

Jenkins Python Pipeline Tutorial - Youtube \
================================== \
https://www.youtube.com/watch?v=6njM8g5hKuk \
https://github.com/vastevenson/jenkins-python-build-test-demo-vs \
https://github.com/vastevenson/pytest-intro-vs


Using Jenkins to auto deploy webservices on Raspberry Pi with Docker \
========================================================= \
https://hackinggate.com/blog/using-jenkins-to-auto-deploy-on-raspberrypi 


How to Install and Run Jenkins With Docker Compose \
=========================================== \
https://www.cloudbees.com/blog/how-to-install-and-run-jenkins-with-docker-compose#running-jenkins-with-docker-compose
<hr>